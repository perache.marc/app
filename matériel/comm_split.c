#include <mpi.h>

int N=1000;

int main(int argc, char** argv){
	MPI_Comm pairimpair; 
	int rang, taille,cle,i; 
	int *tab;
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&taille);
	MPI_Comm_rank(MPI_COMM_WORLD,&rang);
	tab = (int*)malloc(N*sizeof(int));
	for(i = 0; i < N; i++) tab[i]=0;
	if(rang == 2) for(i = 0; i < N; i++) tab[i]=2;
	if(rang == 5) for(i = 0; i < N; i++) tab[i]=5;

	cle = rang;
	if(rang == 2) cle = -1;
	if(rang == 5) cle = -1;

	MPI_Comm_split(MPI_COMM_WORLD,rang%2,cle,pairimpair);

	MPI_Bcast(tab,N,MPI_INT,pairimpair);

	MPI_Comm_free(pairimpair);

	MPI_finalize();	
}
