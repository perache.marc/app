int mutex_trylock (mutex_t ∗ m)
{
 slot_t slot;
 int res;
 spinlock (&(m→lock));
 {
   if (m→nb_thread == 0) {
     m→nb_thread = 1;
     res = 0;
     goto fin;
   } else {
     res = 1;
     goto fin;
   } 

fin:
 }
 spinunlock (&(m→lock));
 return res;
}
