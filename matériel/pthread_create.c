#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
int NB_THREADS;

int* run(int* arg){
 int rank;
 rank = *arg;
 printf("Hello, I’am %d (%p)\n",rank,(void*)pthread_self());
 return arg;
}

int main(int argc, char** argv){
 pthread_t* pids;
 int *ranks;
 int i;
 NB_THREADS=atoi(argv[1]);
 pids = (pthread_t*)malloc(NB_THREADS*sizeof(pthread_t));
 ranks = (int*)malloc(NB_THREADS*sizeof(int));

 for(i = 0; i < NB_THREADS; i++){
   ranks[i] = i;
   pthread_create(&(pids[i]),NULL,(void*(*)(void*))run,&(ranks[i]));
 }

 for(i = 0; i < NB_THREADS; i++){
   int* res;
   pthread_join(pids[i],(void**)(&res));
   fprintf(stderr,"Thread %d Joined\n",*res);
   assert(*res == i);
 }

 return 0;
}
