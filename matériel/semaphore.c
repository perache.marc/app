#include <pthread.h>

typedef struct{
	pthread_mutex_t lock;
	pthread_cond_t cond;
	int value;
} sem_t;

int sem_wait(sem_t* sem){
	pthread_mutex_lock(&(sem->lock));
	if(sem->value < 1){
		pthread_cond_wait(&(sem->cond), &(sem->lock));
	} 
	sem->value --;
	pthread_mutex_unlock(&(sem->lock));
	return 0;
}

int sem_post(sem_t* sem){
	pthread_mutex_lock(&(sem->lock));
	sem->value ++;
	if(sem->value > 0)
		pthread_cond_signal(&(sem->cond));
        pthread_mutex_unlock(&(sem->lock));
	return 0;
}

int sem_init(sem_t* sem, int value){
	pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
        pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
	sem->lock = lock;
	sem->cond = cond;
	sem->value = value; 
	return 0;
}
