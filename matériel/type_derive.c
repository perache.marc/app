#include <mpi.h>

#define N 5
#define M 6

int main(int argc, char ** argv){
	double  mat[N][M];
	int rank;
	MPI_Datatype ligne;
	MPI_Datatype colonne;
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&rang);

	MPI_Type_contiguous(M,MPI_DOUBLE, &ligne);
	MPI_Type_hvector(M,1,&(mat[0][1])-&(mat[0][0]),MPI_DOUBLE, &ligne);
	MPI_Type_commit(ligne);

	MPI_Type_vector(N,1,M,MPI_DOUBLE, &colonne);
	MPI_Type_hvector(N,1,&(mat[2][0])-&(mat[1][0]),MPI_DOUBLE, &colonne);
	MPI_Type_commit(colonne);

	if(rang == 0) MPI_Send(&mat[0][0],1,colonne,1,0,MPI_COMM_WORLD);
	if(rang == 1) MPI_Recv(&mat[0][M-1],1,colonne,1,0,MPI_COMM_WORLD);

	MPI_Type_free(ligne);
	MPI_Type_free(colonne);

	MPI_Finalize();
}
