#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_affiche = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_lire = PTHREAD_COND_INITIALIZER;

volatile char theChar = '\0';

void *lire (void *name)
{
    while(theChar = '\0');
    pthread_mutex_lock(&lock);
    do {
        theChar = getchar ();
	pthread_cond_signal(&cond_affiche);
	pthread_cond_wait(&cond_lire,&lock);
    } while (theChar != 'F');
    pthread_mutex_unlock(&lock);
    return NULL;
}

void *affichage (void *name)
{
    pthread_mutex_lock(&lock);
    theChar = 'a';
    do {
	pthread_cond_wait(&cond_affiche,&lock);
        printf ("car = %c\n", theChar);
	pthread_cond_signal(&cond_lire);
    } while (theChar != 'F');
    pthread_mutex_unlock(&lock);
    return NULL;
}


int main (void)
{
    pthread_t filsA, filsB;
    if (pthread_create (&filsA, NULL, affichage, "AA")) {
        perror ("pthread create");
        exit (EXIT_FAILURE);
    }
    if (pthread_create (&filsB, NULL, lire, "BB")) {
        perror ("pthread create");
        exit (EXIT_FAILURE);
    }
    if (pthread_join (filsA, NULL))
        perror ("pthread join");
    if (pthread_join (filsB, NULL))
        perror ("pthread join");
    printf ("Fin du pere\n");
    return (EXIT_SUCCESS);
}

