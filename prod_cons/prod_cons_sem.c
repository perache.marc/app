#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>

sem_t semaphore_lire;
sem_t semaphore_affiche;
volatile char theChar = '\0';

void *lire (void *name)
{
    do {
	sem_wait(&semaphore_lire);
        theChar = getchar ();
	sem_post(&semaphore_affiche);
    } while (theChar != 'F');
    return NULL;
}

void *affichage (void *name)
{
    do {
	sem_wait(&semaphore_affiche);
        printf ("car = %c\n", theChar);
	sem_post(&semaphore_lire);
    } while (theChar != 'F');
    return NULL;
}


int main (void)
{
    pthread_t filsA, filsB;
    sem_init(&semaphore_lire,0,1);
    sem_init(&semaphore_affiche,0,0);
    if (pthread_create (&filsA, NULL, affichage, "AA")) {
        perror ("pthread create");
        exit (EXIT_FAILURE);
    }
    if (pthread_create (&filsB, NULL, lire, "BB")) {
        perror ("pthread create");
        exit (EXIT_FAILURE);
    }
    if (pthread_join (filsA, NULL))
        perror ("pthread join");
    if (pthread_join (filsB, NULL))
        perror ("pthread join");
    printf ("Fin du pere\n");
    return (EXIT_SUCCESS);
}

